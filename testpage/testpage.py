#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This script exists to reuse HTML files from jekyll-theme-centos/src/_includes
# with expansion of jinja varialbes enable. The expansion occurs using the
# information provided by jekyll-theme-centos/src/_data/centos/*.yml files. Use
# this script to build webpages consistents with latest code published at
# jekyll-theme-centos definitions.
#
# Copyright (C) 2021 Alain Reguera Delgado
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import glob
import yaml
from datetime import datetime
from jinja2 import Environment, FileSystemLoader

jekyll_src_dir = os.environ['JEKYLL_SRC_DIR']

source_file = sys.argv[1]
target_file = sys.argv[2]

configuration = {}
jekyll_data_files = glob.glob(jekyll_src_dir + "/_data/centos/*.yml", recursive=True)
for jekyll_data_file in jekyll_data_files:
    key = os.path.basename(jekyll_data_file).replace(".yml", "")
    value = open(jekyll_data_file)
    configuration[key] = yaml.load(value, Loader=yaml.FullLoader)

# Define template variables you want to expand in the final file. In order to
# reuse jekyll _includes, it is necessary to keep compatibility with the way
# jekyll retrieves dynamic information from _data directory. This is using the
# site dictionary.
site = {}
site["year"] = datetime.now().strftime('%Y')
site["data"] = {}
site["data"]["centos"] = configuration

# Expand template.
html_template_loader = FileSystemLoader("./")
html_template_loader_env = Environment(loader=html_template_loader)
html_template = html_template_loader_env.get_template(source_file)
html_template_rendered = html_template.render(site=site, jekyll_src_dir=jekyll_src_dir)

# Write expanded template in final file.
html = open(target_file, "w")
html.write(html_template_rendered)
